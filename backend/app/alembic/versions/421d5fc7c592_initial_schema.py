"""Initial schema

Revision ID: 421d5fc7c592
Revises: 
Create Date: 2019-12-11 13:19:19.550440

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "421d5fc7c592"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "section",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("title", sa.String(255)),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "question",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("section_id", sa.Integer()),
        sa.Column("prev_id", sa.Integer()),
        sa.Column("question", sa.Text()),
        sa.Column("answer", sa.Text()),
        sa.Column("is_available", sa.Boolean()),
        sa.Column("is_important", sa.Boolean()),
        sa.Index("ix_question_section", "section_id"),
        sa.Index("ix_question_prev", "prev_id"),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "exam_question",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("section_id", sa.Integer()),
        sa.Column("prev_id", sa.Integer()),
        sa.Column("type", sa.Enum('single', 'multi')),
        sa.Column("variants", sa.JSON()),
        sa.Column("correct_variants", sa.JSON()),
        sa.Column("is_available", sa.Boolean()),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("exam_question")
    op.drop_table("question")
    op.drop_table("section")
