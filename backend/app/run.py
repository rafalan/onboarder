#!/usr/bin/env python

import os
import logging
from flask_failsafe import failsafe
from onboarder.factory import create_app


@failsafe
def failsafe_create_app():
    return create_app()


if os.getenv("ONBOARDER_MODE") == "dev":
    logging_format = "%(asctime)s [%(levelname)s] %(name)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG, format=logging_format)
    logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)
    app = failsafe_create_app()
    app.config["DEBUG"] = True
else:
    app = create_app()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
