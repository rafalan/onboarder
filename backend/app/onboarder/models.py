import json
from sqlalchemy import Boolean, Column, Enum, Integer, JSON, String, Text
from .database import Base


class AsDict():
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Section(Base, AsDict):
    __tablename__ = "section"
    id = Column(Integer, primary_key=True)
    title = Column(String(255))


class Question(Base, AsDict):
    __tablename__ = "question"
    id = Column(Integer, primary_key=True)
    section_id = Column(Integer)
    prev_id = Column(Integer)
    question = Column(Text)
    answer = Column(Text)
    is_available = Column(Boolean)
    is_important = Column(Boolean)


class ExamQuestion(Base, AsDict):
    __tablename__ = "exam_question"
    id = Column(Integer, primary_key=True)
    section_id = Column(Integer)
    prev_id = Column(Integer)
    type = Column(Enum("single", "multi"))
    variants = Column(JSON)
    correct_variants = Column(JSON)
    is_available = Column(Boolean)
