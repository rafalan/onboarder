import datetime
import json
import logging
from flask_restplus import Api, Resource, fields
from flask import request, jsonify

from .models import Section, Question, ExamQuestion
from .database import db_session

logger = logging.getLogger(__name__)
api = Api(title="Onboarder API")

section_fields = api.model("Section", {"title": fields.String})

question_fields = api.model(
    "Question",
    {
        "section_id": fields.Integer,
        "prev_id": fields.Integer,
        "question": fields.String,
        "answer": fields.String,
        "is_available": fields.Boolean,
        "is_important": fields.Boolean,
    },
)

exam_question_fields = api.model(
    "Exam Question",
    {
        "section_id": fields.Integer,
        "prev_id": fields.Integer,
        "type": fields.String(description="single/multi"),
        "variants": fields.List(fields.String()),
        "correct_variants": fields.List(fields.Integer()),
        "is_available": fields.Boolean,
    },
)


@api.route("/health")
class HealthResource(Resource):
    def get(self):
        return {"status": "running"}


@api.route("/section")
class SectionResource(Resource):
    @api.expect(section_fields)
    def post(self):
        payload = request.json

        section = Section(title=payload.get("title"))

        db_session.add(section)
        db_session.commit()

        return section.as_dict()

    def get(self):
        sections = Section.query.all()
        return [item.as_dict() for item in sections]


@api.route("/section/<int:section_id>")
class SectionIdResource(Resource):
    @api.expect(section_fields)
    def put(self, section_id):
        payload = request.json

        section = Section.query.filter_by(id=section_id).first()
        section.title = payload.get("title")

        db_session.add(section)
        db_session.commit()

        return section.as_dict()

    def get(self, section_id):
        section = Section.query.filter_by(id=section_id).first()
        return section.as_dict()


@api.route("/question")
class QuestionResource(Resource):
    @api.expect(question_fields)
    def post(self):
        payload = request.json

        question = Question()

        for column in Question.__table__.columns:
            setattr(question, column.name, payload.get(column.name))

        db_session.add(question)
        db_session.commit()

        return question.as_dict()

    def get(self):
        questions = Question.query.all()
        return [item.as_dict() for item in questions]


@api.route("/question/<int:question_id>")
class QuestionIdResource(Resource):
    @api.expect(question_fields)
    def put(self, question_id):
        payload = request.json

        question = Question.query.filter_by(id=question_id).first()

        for column in Question.__table__.columns:
            setattr(question, column.name, payload.get(column.name))

        question.id = question_id

        db_session.add(question)
        db_session.commit()

        return question.as_dict()

    def get(self, question_id):
        question = Question.query.filter_by(id=question_id).first()
        return question.as_dict()


@api.route("/exam-question")
class ExamQuestionResource(Resource):
    @api.expect(exam_question_fields)
    def post(self):
        payload = request.json

        exam_question = ExamQuestion()

        for column in ExamQuestion.__table__.columns:
            setattr(exam_question, column.name, payload.get(column.name))

        db_session.add(exam_question)
        db_session.commit()

        return exam_question.as_dict()

    def get(self):
        exam_questions = ExamQuestion.query.all()
        return [item.as_dict() for item in exam_questions]


@api.route("/exam-question/<int:exam_question_id>")
class ExamQuestionIdResource(Resource):
    @api.expect(exam_question_fields)
    def put(self, exam_question_id):
        payload = request.json

        exam_question = ExamQuestion.query.filter_by(id=exam_question_id).first()

        for column in ExamQuestion.__table__.columns:
            setattr(exam_question, column.name, payload.get(column.name))

        exam_question.id = exam_question_id

        db_session.add(exam_question)
        db_session.commit()

        return exam_question.as_dict()

    def get(self, exam_question_id):
        exam_question = ExamQuestion.query.filter_by(id=exam_question_id).first()
        return exam_question.as_dict()
