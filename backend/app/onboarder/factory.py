# import htmlmin
import logging
import os
# from functools import lru_cache
# import re
# import onboarder.web.assets as assets
# from datetime import datetime
from flask import Flask, request
# from flask_session import Session
# from raven.contrib.flask import Sentry
# from onboarder.web.redis import redis_sessions
# from onboarder.web.views.public import public
# from onboarder.web.database import db_session
# from onboarder.web.views.admin import admin
# from .models import User, Track, Weather

logger = logging.getLogger(__name__)
# sess = Session()


def create_app():
    app = Flask(__name__)

    from .api import api
    api.init_app(app)

    return app
