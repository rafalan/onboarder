const bloom = [
  {name: 'Knowledge', verbs: ['define', 'list', 'state', 'identify'], color: 'blue'},
  {name: 'Comprehension', verbs: ['explain', 'predict', 'interpret', 'convert', 'give an example of', 'summarize'], color: 'purple'},
  {name: 'Application', verbs: ['modify', 'demonstrate', 'apply'], color: 'red'},
  {name: 'Analysis', verbs: ['compare', 'contrast', 'distinguish'], color: 'orange'},
  {name: 'Synthesis', verbs: ['design', 'construct', 'develop', 'formulate'], color: 'green'},
  {name: 'Evaluation', verbs: ['justify', 'evaluate', 'judge'], color: 'orange'}
]

export default {
  sectionId: '',
  sectionTitle: '',
  goalsStatements: [],
  examQuestions: [],
  questions: [],
  bloom
}
