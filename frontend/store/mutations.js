export const SET_SECTION_ID = 'SET_SECTION_ID'
export const SET_SECTION_TITLE = 'SET_SECTION_TITLE'
export const SET_GOALS_STATEMENTS = 'SET_GOALS_STATEMENTS'
export const SET_EXAM_QUESTIONS = 'SET_EXAM_QUESTIONS'
export const SET_QUESTIONS = 'SET_QUESTIONS'

export default {
  [SET_SECTION_ID] (state, sectionId) {
    state.sectionId = sectionId
  },
  [SET_SECTION_TITLE] (state, title) {
    state.sectionTitle = title
  },
  [SET_GOALS_STATEMENTS] (state, goalsStatements) {
    state.goalsStatements = goalsStatements
  },
  [SET_EXAM_QUESTIONS] (state, examQuestions) {
    state.examQuestions = examQuestions
  },
  [SET_QUESTIONS] (state, questions) {
    state.questions = questions
  }
}
