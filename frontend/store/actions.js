import {
  SET_SECTION_ID,
  SET_SECTION_TITLE,
  SET_GOALS_STATEMENTS,
  SET_EXAM_QUESTIONS,
  SET_QUESTIONS
} from "./mutations";
import api from '../api'

export default {
  saveSection: ({ commit }, section) => {
    return api.createNewSection(section).then(response => {
      commit(SET_SECTION_ID, response.data.id)
      commit(SET_SECTION_TITLE, response.data.title)
    })
  },
  saveGoalsStatements: ({ commit }, goalsStatements) => {
    commit(SET_GOALS_STATEMENTS, goalsStatements)
  },
  saveExamQuestion: ({ commit, state }, question) => {
    return api.createNewExamQuestion(question).then(response => {
      let questions = state.examQuestions
      questions.push(question)
      commit(SET_EXAM_QUESTIONS, questions)
      return response.data.id
    })
  },
  saveQuestion: ({ commit, state }, question) => {
    return api.createNewQuestion(question).then(response => {
      return response.data.id
    })
  },
  fetchQuestions: ({ commit }) => {
    return api.fetchQuestions().then(response => {
      commit(SET_QUESTIONS, response.data)
    })
  }
}
