const axios = require('axios').default;

export default {
  createNewSection: (section) => {
    return axios.post('/section', section)
  },
  createNewExamQuestion: (question) => {
    return axios.post('/exam-question', question)
    // {
    //   "section_id": 0,
    //   "prev_id": 0,
    //   "type": "string",
    //   "variants": [
    //   "string"
    // ],
    //   "correct_variants": [
    //   0
    // ],
    //   "is_available": true
    // }
  },
  createNewQuestion: (question) => {
    return axios.post('/question', question)
    // {
    //   "section_id": 0,
    //   "prev_id": 0,
    //   "question": "string",
    //   "answer": "string",
    //   "is_available": true,
    //   "is_important": true
    // }
  },
  fetchQuestions: () => {
    return axios.get('/question')
  }
}
